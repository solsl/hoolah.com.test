/**
 *
 */
package hoolah.com.test;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.io.StringReader;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;

import org.junit.Test;

import hoolah.com.test.parser.RecordParser;

/**
 * @author Vyacheslav Soldatov <vyacheslav.soldatov@inbox.ru>
 *
 */
public class StatisticsCalculatorTest {
    private DateFormat dateParser = RecordParser.createDateFormat();
    private Storage storage = new InMemoryStorage();

    /**
     * Default constructor.
     */
    public StatisticsCalculatorTest() {
        super();
    }

    @Test
    public void testIgnoreMerchant() throws IOException, ParseException {
        final StringBuilder csv = new StringBuilder();
        csv.append("ID, Date, Amount, Merchant, Type, Related Transaction").append('\n');
        csv.append("WLMFRDGD, 20/08/2018 12:45:33, 59.99, Kwik-E-Mart, PAYMENT,").append('\n');
        csv.append("YGXKOEIA, 20/08/2018 12:46:17, 10.95, Kwik-E-Mart, PAYMENT,").append('\n');
        csv.append("LFVCTEYM, 20/08/2018 12:50:02, 5.00, MacLaren, PAYMENT,").append('\n');
        csv.append("SUOVOISP, 20/08/2018 13:12:22, 5.00, Kwik-E-Mart, PAYMENT,").append('\n');
        csv.append("AKNBVHMN, 20/08/2018 13:14:11, 10.95, Kwik-E-Mart, REVERSAL, YGXKOEIA").append('\n');
        csv.append("JYAPKZFZ, 20/08/2018 14:07:10, 99.50, MacLaren, PAYMENT,").append('\n');

        final StatisticsCalculator calc = new StatisticsCalculator(new StringReader(csv.toString()), storage,
                dateParser.parse("20/08/2018 12:45:33"), dateParser.parse("20/08/2018 14:07:10"),
                "arbracadabra");
        final Statistics stats = calc.calculate();
        assertEquals(0, stats.getNumberOfTransactions());
    }
    @Test
    public void testTimeFrames() throws IOException, ParseException {
        final StringBuilder csv = new StringBuilder();
        csv.append("ID, Date, Amount, Merchant, Type, Related Transaction").append('\n');
        csv.append("WLMFRDGD, 20/08/2018 12:45:33, 59.99, Kwik-E-Mart, PAYMENT,").append('\n');
        csv.append("YGXKOEIA, 20/08/2018 12:46:17, 10.95, Kwik-E-Mart, PAYMENT,").append('\n');
        csv.append("SUOVOISP, 20/08/2018 13:12:22, 5.00, Kwik-E-Mart, PAYMENT,").append('\n');
        csv.append("AKNBVHMN, 20/08/2018 13:14:11, 10.95, Kwik-E-Mart, REVERSAL, YGXKOEIA").append('\n');

        final StatisticsCalculator calc = new StatisticsCalculator(
                new StringReader(csv.toString()),storage,
                dateParser.parse("20/08/2018 12:46:17"), dateParser.parse("20/08/2018 13:12:22"),
                "Kwik-E-Mart");
        final Statistics stats = calc.calculate();
        assertEquals(1, stats.getNumberOfTransactions());
    }
    @Test
    public void testExample() throws ParseException, IOException {
        final StringBuilder csv = new StringBuilder();
        csv.append("ID, Date, Amount, Merchant, Type, Related Transaction").append('\n');
        csv.append("WLMFRDGD, 20/08/2018 12:45:33, 59.99, Kwik-E-Mart, PAYMENT,").append('\n');
        csv.append("YGXKOEIA, 20/08/2018 12:46:17, 10.95, Kwik-E-Mart, PAYMENT,").append('\n');
        csv.append("SUOVOISP, 20/08/2018 13:12:22, 5.00, Kwik-E-Mart, PAYMENT,").append('\n');
        csv.append("AKNBVHMN, 20/08/2018 13:14:11, 10.95, Kwik-E-Mart, REVERSAL, YGXKOEIA").append('\n');

        final StatisticsCalculator calc = new StatisticsCalculator(new StringReader(
                csv.toString()), storage,
                dateParser.parse("20/08/2018 12:00:00"), dateParser.parse("20/08/2018 13:00:00"),
                "Kwik-E-Mart");
        final Statistics stats = calc.calculate();
        assertEquals(1, stats.getNumberOfTransactions());
        assertEquals(new BigDecimal("59.99"), stats.getAverage());
    }
    @Test
    public void testCalculate() throws IOException, ParseException {
        final StringBuilder csv = new StringBuilder();
        csv.append("ID, Date, Amount, Merchant, Type, Related Transaction").append('\n');
        csv.append("WLMFRDGD, 20/08/2018 12:45:33, 59.99, Kwik-E-Mart, PAYMENT,").append('\n');
        csv.append("YGXKOEIA, 20/08/2018 12:46:17, 10.95, Kwik-E-Mart, PAYMENT,").append('\n');
        csv.append("SUOVOISP, 20/08/2018 13:12:22, 5.00, Kwik-E-Mart, PAYMENT,").append('\n');
        csv.append("AKNBVHMN, 20/08/2018 13:14:11, 10.95, Kwik-E-Mart, REVERSAL, YGXKOEIA").append('\n');

        final StatisticsCalculator calc = new StatisticsCalculator(
                new StringReader(csv.toString()), storage,
                dateParser.parse("20/08/2018 12:45:33"), dateParser.parse("20/08/2018 13:14:11"),
                "Kwik-E-Mart");

        final Statistics stats = calc.calculate();
        //test average
        BigDecimal avg = new BigDecimal("59.99");
        avg = avg.add(new BigDecimal("5.00"));
        avg = avg.divide(new BigDecimal(2));

        assertEquals(avg, stats.getAverage());
    }
}
