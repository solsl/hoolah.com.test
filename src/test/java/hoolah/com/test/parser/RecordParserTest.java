/**
 *
 */
package hoolah.com.test.parser;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.io.StringReader;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.util.LinkedList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import hoolah.com.test.Transaction;
import hoolah.com.test.TransactionType;

/**
 * @author Vyacheslav Soldatov <vyacheslav.soldatov@inbox.ru>
 *
 */
public class RecordParserTest {
    private RecordParser parser;
    private DateFormat dataFormatter;

    /**
     * Default constructor.
     */
    public RecordParserTest() {
        super();
    }

    @Before
    public void setUp() {
        this.parser = new RecordParser();
        dataFormatter = RecordParser.createDateFormat();
    }

    @Test
    public void testParse() throws IOException {
        final StringBuilder csv = new StringBuilder();
        csv.append("ID, Date, Amount, Merchant, Type, Related Transaction").append('\n');
        csv.append("WLMFRDGD, 20/08/2018 12:45:33, 59.99, Kwik-E-Mart, PAYMENT,").append('\n');
        csv.append("YGXKOEIA, 20/08/2018 12:46:17, 10.95, Kwik-E-Mart, PAYMENT,").append('\n');
        csv.append("LFVCTEYM, 20/08/2018 12:50:02, 5.00, MacLaren, PAYMENT,").append('\n');
        csv.append("SUOVOISP, 20/08/2018 13:12:22, 5.00, Kwik-E-Mart, PAYMENT,").append('\n');
        csv.append("AKNBVHMN, 20/08/2018 13:14:11, 10.95, Kwik-E-Mart, REVERSAL, YGXKOEIA").append('\n');
        csv.append("JYAPKZFZ, 20/08/2018 14:07:10, 99.50, MacLaren, PAYMENT,").append('\n');

        final List<Transaction> transactions = parse(new StringReader(csv.toString()));
        assertEquals(6, transactions.size());

        //test one
        final Transaction t = transactions.get(4);
        assertEquals(new BigDecimal("10.95"), t.getAmount());
        assertEquals("20/08/2018 13:14:11", this.dataFormatter.format(t.getDate()));
        assertEquals("AKNBVHMN", t.getId());
        assertEquals("Kwik-E-Mart", t.getMerchant());
        assertEquals("YGXKOEIA", t.getRelatedTransactionId());
        assertEquals(TransactionType.REVERSAL, t.getType());
    }
    @Test
    public void testOtherColumnOrder() throws IOException {
        final StringBuilder csv = new StringBuilder();
        csv.append("Date, Amount, Merchant, Type, ID, Related Transaction").append('\n');
        csv.append("20/08/2018 13:14:11, 10.95, Kwik-E-Mart, REVERSAL, AKNBVHMN, YGXKOEIA").append('\n');

        final List<Transaction> transactions = parse(new StringReader(csv.toString()));

        //test one
        final Transaction t = transactions.get(0);
        assertEquals(new BigDecimal("10.95"), t.getAmount());
        assertEquals("20/08/2018 13:14:11", this.dataFormatter.format(t.getDate()));
        assertEquals("AKNBVHMN", t.getId());
        assertEquals("Kwik-E-Mart", t.getMerchant());
        assertEquals("YGXKOEIA", t.getRelatedTransactionId());
        assertEquals(TransactionType.REVERSAL, t.getType());
    }

    private List<Transaction> parse(final StringReader stringReader) throws IOException {
        final List<Transaction> trs = new LinkedList<>();
        parser.setRecordHandler(r -> trs.add(r));
        parser.parse(stringReader);
        return trs;
    }
}
