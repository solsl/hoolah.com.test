/**
 *
 */
package hoolah.com.test;

import java.math.BigDecimal;

/**
 * @author Vyacheslav Soldatov <vyacheslav.soldatov@inbox.ru>
 *
 */
public interface Storage {
    void addTransaction(Transaction tr);
    void removeTransaction(String id);
    int getNumberOfTransactions();
    BigDecimal getAverage();
}
