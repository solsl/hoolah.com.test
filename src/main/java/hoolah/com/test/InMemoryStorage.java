/**
 *
 */
package hoolah.com.test;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Vyacheslav Soldatov <vyacheslav.soldatov@inbox.ru>
 *
 */
public class InMemoryStorage implements Storage {
    private final Map<String, Transaction> transactions = new HashMap<>();

    /**
     * Default constructor.
     */
    public InMemoryStorage() {
        super();
    }

    @Override
    public void addTransaction(final Transaction tr) {
        transactions.put(tr.getId(), tr);
    }

    @Override
    public void removeTransaction(final String id) {
        transactions.remove(id);
    }

    @Override
    public int getNumberOfTransactions() {
        return transactions.size();
    }

    @Override
    public BigDecimal getAverage() {
        BigDecimal avg = new BigDecimal(0);
        if (transactions.size() == 0) {
            return avg;
        }

        for (final Map.Entry<String, Transaction> e : transactions.entrySet()) {
            avg = avg.add(e.getValue().getAmount());
        }
        return avg.divide(new BigDecimal(transactions.size()));
    }
}
