/**
 *
 */
package hoolah.com.test;

import java.math.BigDecimal;

/**
 * @author Vyacheslav Soldatov <vyacheslav.soldatov@inbox.ru>
 *
 */
public class Statistics {
    /**
     * Total number of transactions.
     */
    private int numberOfTransactions;
    /**
     * Transactions average exclude reverted.
     */
    private BigDecimal average;

    /**
     * @param numberOfTransactions total number of transactions.
     */
    public void setNumberOfTransactions(final int numberOfTransactions) {
        this.numberOfTransactions = numberOfTransactions;
    }
    /**
     * @return total number of transactions.
     */
    public int getNumberOfTransactions() {
        return numberOfTransactions;
    }
    /**
     * @param avg average.
     */
    public void setAverage(final BigDecimal avg) {
        this.average = avg;
    }
    /**
     * @return the average
     */
    public BigDecimal getAverage() {
        return average;
    }
}
