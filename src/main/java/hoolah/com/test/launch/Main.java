/**
 *
 */
package hoolah.com.test.launch;

import java.io.File;
import java.io.FileReader;
import java.io.Reader;
import java.text.DateFormat;
import java.util.Date;

import hoolah.com.test.InMemoryStorage;
import hoolah.com.test.Statistics;
import hoolah.com.test.StatisticsCalculator;
import hoolah.com.test.parser.RecordParser;

/**
 * @author Vyacheslav Soldatov <vyacheslav.soldatov@inbox.ru>
 *
 */
public class Main {
    /**
     * Default constructor.
     */
    public Main() {
        super();
    }

    /**
     * Given the above CSV file and the following input arguments:
     * fromDate: 20/08/2018 12:00:00
     * toDate: 20/08/2018 13:00:00
     *
     * @param args program arguments.
     */
    public static void main(final String[] args) throws Exception {
        if (args.length != 4) {
            System.err.println("Invalid program arguments. Example:");
            System.err.println("java -jar main.jar /path/to/file.csv"
                    + " \"20/08/2018 12:00:00\" \"20/08/2018 13:00:00\" \"Merchant\"");
            return;
        }

        final File csv = new File(args[0]);
        if (!csv.exists()) {
            System.err.println("Input file " + args[0] + " not exists");
            return;
        }

        final DateFormat fmt = RecordParser.createDateFormat();
        final Date startDate = fmt.parse(args[1]);
        final Date endDate = fmt.parse(args[2]);
        final String merchant = args[3];

        final Reader r = new FileReader(csv);
        final Statistics stats;
        try {
            final StatisticsCalculator calc = new StatisticsCalculator(
                    r, new InMemoryStorage(), startDate, endDate, merchant);
            stats = calc.calculate();
        }  finally {
            r.close();
        }

        System.out.println("Number of transactions = " + stats.getNumberOfTransactions());
        System.out.println("Average Transaction Value = " + stats.getAverage());
    }
}
