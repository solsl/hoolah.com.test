/**
 *
 */
package hoolah.com.test;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author Vyacheslav Soldatov <vyacheslav.soldatov@inbox.ru>
 *
 */
public class Transaction {
    /**
     * ID - A string representing the transaction id
     */
    private String id;
    /**
     * Date - The date and time when the transaction took place (format "DD/MM/YYYY hh:mm:ss")
     */
    private Date date;
    /**
     * Amount - The value of the transaction (dollars and cents)
     */
    private BigDecimal amount;
    /**
     * Merchant - The name of the merchant this transaction belongs to
     */
    private String merchant;
    /**
     * Type - The type of the transaction, which could be either PAYMENT or REVERSAL
     */
    private TransactionType type;
    /**
     * Related Transaction - (Optional) - In the case a REVERSAL transaction, this field will
     * contain the ID of the transaction it is reversing.
     */
    private String relatedTransactionId;

    /**
     * Default constructor.
     */
    public Transaction() {
        super();
    }
    /**
     * @return transaction ID.
     */
    public String getId() {
        return id;
    }
    /**
     * @param id transaction ID.
     */
    public void setId(final String id) {
        this.id = id;
    }
    /**
     * @return date and time when the transaction took place
     */
    public Date getDate() {
        return date;
    }
    /**
     * @param date date and time when the transaction took place
     */
    public void setDate(final Date date) {
        this.date = date;
    }
    /**
     * @return value of the transaction (dollars and cents)
     */
    public BigDecimal getAmount() {
        return amount;
    }
    /**
     * @param amount value of the transaction (dollars and cents)
     */
    public void setAmount(final BigDecimal amount) {
        this.amount = amount;
    }
    /**
     * @return name of the merchant this transaction belongs to
     */
    public String getMerchant() {
        return merchant;
    }
    /**
     * @param name of the merchant this transaction belongs to
     */
    public void setMerchant(final String merchant) {
        this.merchant = merchant;
    }
    /**
     * @return transaction type.
     */
    public TransactionType getType() {
        return type;
    }
    /**
     * @param transactoin type
     */
    public void setType(final TransactionType type) {
        this.type = type;
    }
    /**
     * @return In the case a REVERSAL transaction, this field will
     * contain the ID of the transaction it is reversing.
     */
    public String getRelatedTransactionId() {
        return relatedTransactionId;
    }
    /**
     * @param tr In the case a REVERSAL transaction, this field will
     * contain the ID of the transaction it is reversing.
     */
    public void setRelatedTransactionId(final String tr) {
        this.relatedTransactionId = tr;
    }
    @Override
    public String toString() {
        return date + ": " + getAmount() + " " + getType();
    }
}
