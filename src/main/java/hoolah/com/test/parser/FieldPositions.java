/**
 *
 */
package hoolah.com.test.parser;

/**
 * @author Vyacheslav Soldatov <vyacheslav.soldatov@inbox.ru>
 *
 */
public class FieldPositions {
    /**
     * ID - A string representing the transaction id
     */
    private int id = 0;
    /**
     * Date - The date and time when the transaction took place (format "DD/MM/YYYY hh:mm:ss")
     */
    private int date = 1;
    /**
     * Amount - The value of the transaction (dollars and cents)
     */
    private int amount = 2;
    /**
     * Merchant - The name of the merchant this transaction belongs to
     */
    private int merchant = 3;
    /**
     * Type - The type of the transaction, which could be either PAYMENT or REVERSAL
     */
    private int type = 4;
    /**
     * Related Transaction - (Optional) - In the case a REVERSAL transaction, this field will
     * contain the ID of the transaction it is reversing.
     */
    private int relatedTransactionId = 5;

    /**
     * The field order
     */
    public FieldPositions() {
        super();
    }

    /**
     * @return ID position
     */
    public int getId() {
        return id;
    }
    /**
     * @param ID position
     */
    public void setId(final int position) {
        this.id = position;
    }
    /**
     * @return date position
     */
    public int getDate() {
        return date;
    }
    /**
     * @param position position
     */
    public void setDate(final int position) {
        this.date = position;
    }
    /**
     * @return amount position
     */
    public int getAmount() {
        return amount;
    }
    /**
     * @param position amount position
     */
    public void setAmount(final int position) {
        this.amount = position;
    }
    /**
     * @return merchant position.
     */
    public int getMerchant() {
        return merchant;
    }
    /**
     * @param position merchant position
     */
    public void setMerchant(final int position) {
        this.merchant = position;
    }
    /**
     * @return type position
     */
    public int getType() {
        return type;
    }
    /**
     * @param position type position
     */
    public void setType(final int position) {
        this.type = position;
    }
    /**
     * @return related transaction ID position
     */
    public int getRelatedTransactionId() {
        return relatedTransactionId;
    }
    /**
     * @param position related transaction ID position
     */
    public void setRelatedTransactionId(final int position) {
        this.relatedTransactionId = position;
    }
}
