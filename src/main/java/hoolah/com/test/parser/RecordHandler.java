/**
 *
 */
package hoolah.com.test.parser;

import hoolah.com.test.Transaction;

/**
 * @author Vyacheslav Soldatov <vyacheslav.soldatov@inbox.ru>
 *
 */
@FunctionalInterface
public interface RecordHandler {
    boolean handle(Transaction tr);
}
