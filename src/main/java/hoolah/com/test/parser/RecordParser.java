/**
 *
 */
package hoolah.com.test.parser;

import java.io.IOException;
import java.io.Reader;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.opencsv.CSVReader;

import hoolah.com.test.Transaction;
import hoolah.com.test.TransactionType;

/**
 * @author Vyacheslav Soldatov <vyacheslav.soldatov@inbox.ru>
 *
 */
public class RecordParser {
    private RecordHandler handler;
    private FieldPositions header;
    private DateFormat dateParser;

    /**
     * Default constructor.
     */
    public RecordParser() {
        super();
        dateParser = createDateFormat();
        //set default field positions
        setFieldPositions(new FieldPositions());
    }

    /**
     * @return date format.
     */
    public static SimpleDateFormat createDateFormat() {
        return new SimpleDateFormat("dd/MM/yyyy' 'HH:mm:ss");
    }
    /**
     * @param record transaction record in CSV format.
     * @return transaction.
     */
    public Transaction buildRecord(final String[] record) {
        final FieldPositions pos = getFieldPositions();
        final Transaction t = new Transaction();

        t.setAmount(parseAmount(record[pos.getAmount()]));
        t.setDate(parseDate(record[pos.getDate()]));
        t.setId(parseId(record[pos.getId()]));
        t.setMerchant(parseMerchant(record[pos.getMerchant()]));
        t.setRelatedTransactionId(parseRelatedTransactionId(record[pos.getRelatedTransactionId()]));
        t.setType(parseType(record[pos.getType()]));
        return t;
    }
    /**
     * @param str string.
     * @return transaction type.
     */
    private TransactionType parseType(final String str) {
        return TransactionType.valueOf(str.toUpperCase());
    }
    /**
     * @param str string.
     * @return related transaction ID.
     */
    private String parseRelatedTransactionId(final String str) {
        final String id = str.length() > 0 ? str : null;
        //TODO validate transaction ID.
        return id;
    }
    /**
     * @param str string.
     * @return merchant.
     */
    private String parseMerchant(final String str) {
        //TODO validate merchant.
        return str;
    }
    /**
     * @param str string.
     * @return ID.
     */
    private String parseId(final String str) {
        //TODO validate transaction ID.
        return str;
    }
    /**
     * @param str sting.
     * @return parsed date.
     */
    private Date parseDate(final String str) {
        try {
            return dateParser.parse(str);
        } catch (final ParseException e) {
            throw new RuntimeException("Incorrect date format of " + str);
        }
    }
    /**
     * @param str string.
     * @return amount as big decimal.
     */
    private BigDecimal parseAmount(final String str) {
        return new BigDecimal(str);
    }

    /**
     * @param header CSV header.
     * @return object containing transaction field positions.
     */
    public FieldPositions buildHeader(final String[] header) {
        final FieldPositions p = new FieldPositions();
        p.setId(indexOfIgnoreCase(header, "ID"));
        p.setDate(indexOfIgnoreCase(header, "Date"));
        p.setAmount(indexOfIgnoreCase(header, "Amount"));
        p.setMerchant(indexOfIgnoreCase(header, "Merchant"));
        p.setType(indexOfIgnoreCase(header, "Type"));
        p.setRelatedTransactionId(indexOfIgnoreCase(header, "Related Transaction"));
        return p;
    }
    /**
     * @param reader CSV stream reader.
     * @throws IOException
     */
    public void parse(final Reader reader) throws IOException {
        final CSVReader r = new CSVReader(reader);

        //read header line
        String[] line = readLine(r);
        if (line == null) {
            //empty file
            return;
        }

        //parse header
        //support situation when is not a header. May be not need, but try to implement it ;-)
        if (isHeader(line)) {
            setFieldPositions(buildHeader(line));
        } else {
            handler.handle(buildRecord(line));
        }

        //parse body
        while ((line = readLine(r)) != null) {
            try {
                checkColumnNumber(line);
                if (handler != null) {
                    final boolean shouldContinue = handler.handle(buildRecord(line));
                    if (!shouldContinue) {
                        break;
                    }
                }
            } catch (final Exception e) {
                break;
            }
        }
    }

    /**
     * @param r CSV reader.
     * @return line fields.
     * @throws IOException
     */
    private String[] readLine(final CSVReader r) throws IOException {
        final String[] line = r.readNext();
        if (line == null) {
            return null;
        }
        for (int i = 0; i < line.length; i++) {
            line[i] = line[i].trim();
        }
        return line;
    }
    /**
     * @param fields list of header fields.
     * @return true if the fields is header.
     */
    private boolean isHeader(final String[] fields) {
        checkColumnNumber(fields);
        return containsIgnoreCase(fields, "ID")
            && containsIgnoreCase(fields, "Date")
            && containsIgnoreCase(fields, "Amount")
            && containsIgnoreCase(fields, "Merchant")
            && containsIgnoreCase(fields, "Type")
            && containsIgnoreCase(fields, "Related Transaction");
    }
    /**
     * @param fields fields.
     * @param field field.
     * @return true if array contains the field ignore case.
     */
    private boolean containsIgnoreCase(final String[] fields, final String field) {
        return indexOfIgnoreCase(fields, field) > -1;
    }
    /**
     * @param fields fields.
     * @param field field.
     * @return position of field in array of fields ignore case.
     */
    private int indexOfIgnoreCase(final String[] fields, final String field) {
        for (int i = 0; i < fields.length; i++) {
            if (field.equalsIgnoreCase(fields[i])) {
                return i;
            }
        }
        return -1 ;
    }

    /**
     * @param fields array of fields.
     */
    private void checkColumnNumber(final String[] fields) {
        if (fields.length != 6) {
            throw new IllegalStateException("Number of columns "
                    + fields.length + ", expected 6");
        }
    }
    /**
     * @return record handler.
     */
    public RecordHandler getRecordHandler() {
        return handler;
    }
    /**
     * @param handler record handler.
     */
    public void setRecordHandler(final RecordHandler handler) {
        this.handler = handler;
    }
    /**
     * @return field positions.
     */
    public FieldPositions getFieldPositions() {
        return header;
    }
    /**
     * @param field positions.
     */
    public void setFieldPositions(final FieldPositions h) {
        this.header = h;
    }
}
