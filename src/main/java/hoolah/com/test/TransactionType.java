/**
 *
 */
package hoolah.com.test;

/**
 * @author Vyacheslav Soldatov <vyacheslav.soldatov@inbox.ru>
 *
 */
public enum TransactionType {
    PAYMENT,
    REVERSAL
}
