/**
 *
 */
package hoolah.com.test;

import java.io.IOException;
import java.io.Reader;
import java.util.Date;

import hoolah.com.test.parser.RecordParser;

/**
 * @author Vyacheslav Soldatov <vyacheslav.soldatov@inbox.ru>
 *
 */
public class StatisticsCalculator {
    private final Reader reader;
    private final Date startDate;
    private final Date endDate;
    private final String merchant;
    private final Storage storage;

    public StatisticsCalculator(final Reader r, final Storage storage, final Date startDate,
            final Date endDate, final String merchant) {
        super();
        this.storage = storage;
        this.reader = r;
        this.startDate = startDate;
        this.endDate = endDate;
        this.merchant = merchant;
    }

    public Statistics calculate() throws IOException {
        final RecordParser parser = new RecordParser();

        parser.setRecordHandler(r -> processNextRecord(r));
        parser.parse(reader);

        final Statistics stats = new Statistics();
        stats.setNumberOfTransactions(storage.getNumberOfTransactions());
        stats.setAverage(storage.getAverage());
        return stats;
    }

    /**
     * @param tr transaction.
     * @return true if should continue.
     */
    private boolean processNextRecord(final Transaction tr) {
        if (tr.getDate().after(endDate) && tr.getType() != TransactionType.REVERSAL) {
            return true;
        }
        if (merchant.equals(tr.getMerchant()) && !tr.getDate().before(startDate)) {
            doProcessTransaction(tr);
        }
        return true;
    }

    /**
     * @param tr transaction.
     */
    private void doProcessTransaction(final Transaction tr) {
        if (tr.getType() == TransactionType.REVERSAL) {
            storage.removeTransaction(tr.getRelatedTransactionId());
        } else {
            storage.addTransaction(tr);
        }
    }
}
